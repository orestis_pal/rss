<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'Home@dashboard')->name('dashboard');
Route::post('/search', 'Search@postSearch')->name('messages.search.post');
Route::get('/search-results', 'Search@getSearchResults')->name('messages.search.results.get');
