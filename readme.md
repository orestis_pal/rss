# Rss

- composer install
- npm install
- gulp
- php artisan migrate

...and I think it should be up and running.

Apart from the Controllers and the Managers, which should be easy to spot by just looking at "routes/web.php", there is a command "DownloadRss" in "app/Console/Commands/".
This command is scheduled to run daily in "app/Console/Kernel.php".

In order to test the functionality you can just run "php artisan rss:download" and the RSS feeds that are currently hard-coded in the array $feedUrls in the handle() method
in DownloadRss.php, should be fetched and stored in the DB. Every time that command executes it checks if there are new messages in any of the feeds and retrieves them. There
are 20 messages shown in the front-end, with titles, descriptions, dates (in time elapsed since form...), as well as image when available. Those messages are the combined newest 20 messages from all the feeds, sorted by date. There is also a small search form in the sidebar
that searches for a string in the titles and descriptions of the messages in the DB. The results are returned in a paginated view. The descriptions have a max length set, and
the titles can only break at most in two lines, after which the text is cut short and ellipsis (...) are shown.