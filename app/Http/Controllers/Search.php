<?php

namespace App\Http\Controllers;

use App\Http\Requests\MessageSearchRequest;
use App\Rss\Managers\Message as MessageManager;
use Symfony\Component\HttpFoundation\Request;

class Search extends Controller
{
    /** @var MessageManager */
    private $messageManager;

    public function __construct (MessageManager $messageManager) {
        $this->messageManager = $messageManager;
    }

    public function postSearch(MessageSearchRequest $messageSearchRequest)
    {
        // flash parameters to session so the next request can access them
        $messageSearchRequest->session()->flash('query', trim($messageSearchRequest->input('query')));

        // redirect to search results' first page
        return redirect()->route('messages.search.results.get', ['page' => 1]);
    }

    public function getSearchResults(Request $request)
    {
        // get searchParameters from session
        $query = session('query');

        // flash searchParameters to session so the next request can access them
        $request->session()->flash('query', $query);
        $messages = $this->messageManager->search($query, $request->input('page'));

        $viewData = [
            'messages' => $messages,
            'query' => $query
        ];

        return view(
            'search_results',
            $viewData
        );
    }
}
