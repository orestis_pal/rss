<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Support\Facades\View;
use App\Rss\Managers\Message as MessageManager;

class Home extends Controller
{
    /** @var MessageManager */
    private $messageManager;

    public function __construct (MessageManager $messageManager) {
        $this->messageManager = $messageManager;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        $viewData = [
            'messages' => Message::presentable()->newFirst()->latest()->get()
        ];

        return View::make('dashboard', $viewData);
    }
}
