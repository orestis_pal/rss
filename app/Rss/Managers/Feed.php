<?php

namespace App\Rss\Managers;

use SimplePie;

class Feed
{
    /** @var SimplePie */
    private $simplePie;

    public function __construct (SimplePie $simplePie) {
        $this->simplePie = $simplePie;
    }

    public function getFromUrl(string $url)
    {
        $feed = $this->simplePie;
        $feed->enable_cache(false);
        $feed->set_feed_url($url);

        // Initialize the feed object
        $feed->init();

        // This will work if all of the feeds accept the same settings.
        $feed->handle_content_type();

        if ($feed->error())
        {
            throw new \Exception;
        }

        return $feed;
    }

}