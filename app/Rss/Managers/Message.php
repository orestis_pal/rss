<?php

namespace App\Rss\Managers;

use App\Message as MessageModel;

class Message
{
    /** @var MessageModel */
    private $message;

    public function __construct (MessageModel $message) {
        $this->message = $message;
    }

    public function search(string $query, int $page = 1)
    {
        // initial part of query
        $query = $this->message
            ->where('title', 'like', '%' . $query . '%')
            ->orWhere('description', 'like', '%' . $query . '%');

        $results = $query->paginate(5 , ['*'], 'page', $page);

        return $results;
    }
}