<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'feed_id',
        'url',
        'title',
        'description',
        'permalink',
        'image_url',
        'message_date'
    ];

    /**
     * The fields that should be treated as Carbon instances
     *
     * @var array
     */
    protected $dates = [
        'message_date'
    ];

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Returns the description of the message.
     * Shortens to desired length if it is exceeded
     *
     * @return mixed|string
     */
    public function getDescription()
    {
        $lengthLimit = 300;

        if (strlen($this->description) > $lengthLimit) {
            return strip_tags(substr(
                $this->description,
                0,
                strpos($this->description, ' ', $lengthLimit)
            ) . '...');
        }

        return strip_tags($this->description);
    }

    /**
     * @return mixed
     */
    public function getMessageDate()
    {
        return $this->message_date;
    }

    /**
     * @return mixed
     */
    public function getImageUrl()
    {
        return $this->image_url;
    }

    /**
     * @return mixed
     */
    public function getPermalink()
    {
        return $this->permalink;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopePresentable($query)
    {
        return $query->where('title', '!=', null)
            ->where('description', '!=', null);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeLatest($query)
    {
        return $query->take(20);
    }

    public function scopeNewFirst($query)
    {
        return $query->orderBy('message_date', 'desc');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function feed()
    {
        return $this->belongsTo('App\Feed', 'feed_id', 'id');
    }
}
