<?php

namespace App\Console\Commands;

use App\Feed;
use App\Message;
use Illuminate\Console\Command;
use App\Rss\Managers\Feed as FeedManager;
use Carbon\Carbon;

class DownloadRss extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rss:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks for new messages in the specified feeds and retrieves them';

    /** @var FeedManager  */
    private $feedManager;

    /**
     * @param FeedManager $feedManager
     */
    public function __construct(FeedManager $feedManager)
    {
        parent::__construct();

        $this->feedManager = $feedManager;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $feedUrls = [
            'http://rss.nytimes.com/services/xml/rss/nyt/NYRegion.xml',
            'http://rss.nytimes.com/services/xml/rss/nyt/Technology.xml',
            'https://www.wired.com/category/photo/feed/',
        ];

        $this->info('Please wait while the feeds are being updated...');

        $this->persistNewMessagesFromFeeds($feedUrls);
        $this->info('Done.');
    }

    /**
     * @param \SimplePie $feed
     * @param string $feedUrl
     * @return Feed
     */
    private function persistFeed(\SimplePie $feed, string $feedUrl)
    {
        $newFeed = new Feed([
            'url' => $feedUrl,
            'title' => $feed->get_title(),
        ]);

        $newFeed->save();
        return $newFeed;
    }

    /**
     * @param \SimplePie_Item $item
     * @param int $feedId
     * @return Message
     */
    private function persistMessage(\SimplePie_Item $item, int $feedId)
    {
        $image_url = null;

        if ($enclosure = $item->get_enclosure()) {
            $image_url = $enclosure->get_link();
        }

        $message = new Message([
            'feed_id' => $feedId,
            'title' => $item->get_title(),
            'description' => $item->get_description(),
            'permalink' => $item->get_permalink(),
            'image_url' => $image_url,
            'message_date' => $item->get_date('Y-m-d H:i:s')
        ]);

        $message->save();
        return $message;
    }

    /**
     * @param array $feedUrls
     * @throws \Exception
     */
    private function persistNewMessagesFromFeeds(array $feedUrls)
    {
        foreach ($feedUrls as $url) {
            $downloadedFeed = $this->feedManager->getFromUrl($url);
            $feedFromDb = Feed::where('url', $url)->first();

            if (!($feedFromDb instanceof Feed)) {
                $newFeed = $this->persistFeed($downloadedFeed, $url);

                foreach ($downloadedFeed->get_items() as $item) {
                    $newMessage = $this->persistMessage($item, $newFeed->id);
                }
            } else {
                $newestFeedMessageInDb = Message::where('feed_id', $feedFromDb->id)->newfirst()->first();

                foreach ($downloadedFeed->get_items() as $item) {
                    $newMessageDate = Carbon::parse($item->get_date('Y-m-d H:i:s'));

                    $this->persistMessageIfNew($newestFeedMessageInDb, $newMessageDate, $item, $feedFromDb);
                }
            }
        }
    }

    /**
     * @param Message $newestFeedMessageInDb
     * @param Carbon $newMessageDate
     * @param \SimplePie_Item $item
     * @param Feed $feedFromDb
     */
    private function persistMessageIfNew(Message $newestFeedMessageInDb, Carbon $newMessageDate, \SimplePie_Item $item, Feed $feedFromDb)
    {
        if (!$newestFeedMessageInDb->message_date->gte($newMessageDate)) {
            $newMessage = $this->persistMessage($item, $feedFromDb->id);
        }
    }
}
