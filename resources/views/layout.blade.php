<!doctype html>
<html lang="en">
    @include('partials.head')

<body>
    <div class="container">
        @include('partials.header')


        <div class="row">
            <div class="col-md-9">
                @include('partials.main')
            </div>
            <div class="col-md-3 hidden-xs hidden-sm">
                @include('partials.sidebar')
            </div>
        </div>

        @include('partials.footer')
    </div>

    <script>
        /*
         * Application namespace
         *
         * Data that needs to be passed from the PHP application to JS
         * can be defined here
         */
        var DP = {
            baseUrl: '{!! url('/') !!}'
        };
    </script>
    <script src="{{ elixir('js/app.js') }}"></script>
</body>
</html>