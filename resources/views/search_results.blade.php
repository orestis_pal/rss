@extends('layout')


@section('content')

    <h1>Results for search: <em>"{{ $query }}" ({{ $messages->total() }})</em></h1>

    <section>
        @include('partials.messages', ['messages' => $messages])
    </section>


    <div class="pagination__container text-right">
        {!! $messages->render() !!}
    </div>


@endsection