@extends('layout')


@section('content')

    <section>
        @include('partials.messages', ['messages' => $messages])
    </section>

@endsection