<form method="post" action="{{ route('messages.search.post') }}">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <label for="query">Search messages</label>
                <input type="text"
                       id="query"
                       name="query"
                       class="form-control"
                       value="{{ old('query') }}"
                        >
                @include('helpers.forms.error_message', ['field' => 'query'])
             </div>

            <div class="form-group text-right">
                <button type="submit"
                        class="btn btn-default"
                        >
                    Submit</button>
            </div>
        </div>
    </div>
</form>