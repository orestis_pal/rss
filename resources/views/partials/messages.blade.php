@foreach ($messages as $message)
    <div class="Feed-item">
        <h2 class="Feed-item_heading cross-browser-ellipsis">
            <a class="Feed-item_title"
               href="{!! $message->getPermalink() !!}">
                {!! $message->getTitle() !!}
            </a>
        </h2>

        @if(!is_null($message->getImageUrl()))
            <img class="Feed-item_image"
                 src="{!! $message->getImageUrl() !!}"
                 alt="image">
        @endif

        <p class="Feed-item_description">{!! $message->getDescription() !!}</p>

        <p>
            <small>Posted on {!! $message->getMessageDate()->diffForHumans() !!} @ {!! $message->feed->getTitle() !!}</small>
        </p>
    </div>
@endforeach
